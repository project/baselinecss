<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>
<body>
<div id="page">
	<div id="header">
		<div id="header-main" class="column width2 first layout-grid">
			<h1 id="logo-text"><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>
			<p id="slogan"><?php print $site_slogan; ?></p>
		</div>
		<div id="nav" class="column width2">
			<?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
			<?php print $search_box; ?>
		</div>
	</div>
	<div id="content-wrapper">
		<div id="main" class="<?php if($right): ?>column width3 first<?php else: ?> column width4 first<?php endif; ?>">
	        <?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
	        <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
	        <?php if (!empty($messages)): print $messages; endif; ?>
	        <?php if (!empty($help)): print $help; endif; ?>
	        <div id="content-output"> 
	          <?php print $content; ?>
	        </div>
		</div>
		<?php if ($right): ?>
		<div class="column width1">
			<?php print $right; ?>
		</div>
		<?php endif; ?>
	</div>


	<div id="footer">
		<div class="column width2 first" id="footer-left">
			<?php print $footer_left; ?>
        </div>
        <div class="column width2" id="footer-right">
          <?php print $footer_right; ?>
        </div>
	</div>
	<div id="subfooter">
        <div id="footer-meta" class="clear-block">
          <?php if ($footer_message): ?>
          <p class="bottom-left"><?php print $footer_message; ?></p>
          <?php endif; ?>
  
          <?php if ($secondary_links): ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
          <?php endif; ?>
        </div>

        <?php if ($footer): ?>
        <div id="footer-bottom-content">
          <?php print $footer; ?>
        </div>
        <?php endif; ?>
      </div>
      <!-- footer bottom ends here -->

</div>
<!-- page ends here -->
    <?php print $closure; ?>
  </body>
</html>